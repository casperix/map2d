package casperix.map2d

import casperix.map2d.bytes.ByteArray3D
import casperix.math.vector.Vector2i
import kotlin.test.Test
import kotlin.test.assertTrue

class ByteVolumeTest {
	@Test
	fun readWrite() {
		val width = 201
		val height = 117

		val item = ByteArray3D(width, height, 5)
		listOf(true, false).forEach { isWrite ->
			(0 until width).forEach { x ->
				(0 until height).forEach { y ->
					val pixel = byteArrayOf(x.toByte(), -128, 127, y.toByte(), (x * y).toByte())
					val position = Vector2i(x, y)
					if (isWrite) {
						item.setColumn(position, pixel)
					} else {
						val actualPixel = item.getColumn(position)
						assertTrue(pixel.contentEquals(actualPixel))
					}
				}
			}
		}
	}
}