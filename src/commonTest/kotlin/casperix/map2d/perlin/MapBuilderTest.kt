package casperix.map2d.perlin

import casperix.map2d.DoubleMap2D
import casperix.math.vector.Vector2i
import casperix.misc.time.executeAndMeasure
import kotlin.test.Test

class MapBuilderTest {
	@Test
	fun createMapXY() {
		val dimension = Vector2i(4096)
		val (time, map) = executeAndMeasure {
			DoubleMap2D.createByXY(dimension) { pos ->
				(pos.x + pos.y).toDouble()
			}
		}
		println("Map $dimension for $time ms")
	}
}