package casperix.map2d.perlin


import casperix.map2d.DoubleMap2D
import casperix.math.axis_aligned.Box2i
import casperix.math.function.perlin.ValueNoise2d
import casperix.math.interpolation.hermiteInterpolate
import casperix.math.vector.Vector2i
import casperix.math.vector.Vector3d
import casperix.misc.time.executeAndMeasure
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ValueNoise2dTest {

	@Test
	fun noiseTest() {
		val noise = ValueNoise2d()
		val a = noise.output(0.0, 0.0)
		val b = noise.output(5.0, 0.0)
		val c = noise.output(15.0, 15.0)

		assertEquals(-0.5635819677263498, a)
		assertEquals(0.07084455780100589, b)
		assertEquals(-0.6289741178572762, c)
	}

	@Test
	fun noiseCachesTest() {
		val noise = ValueNoise2d(interpolateFunction = hermiteInterpolate)
		val cachedNoise = CachedValueNoise2d(noise, Box2i(Vector2i.ZERO, Vector2i.XY * 15))
		val a = cachedNoise.output(0.0, 0.0)
		val b = cachedNoise.output(5.0, 0.0)
		val c = cachedNoise.output(15.0, 15.0)

		assertEquals(-0.5635819677263498, a)
		assertEquals(0.07084455780100589, b)
		assertEquals(-0.6289741178572762, c)
	}

	@Test
	fun noisePerformanceTest() {
		val (time1, result1) = executeAndMeasure {
			(1..1).forEach { seed ->
				val dimension = Vector2i(1024)
				val noise = ValueNoise2d(scale = Vector3d(1.0/256.0, 1.0/256.0, seed.toDouble()), interpolateFunction = hermiteInterpolate)
				DoubleMap2D.createByXY(dimension) {
					noise.output(it.x.toDouble(), it.y.toDouble())
				}
			}
		}
		println("Noise 10^6 create for " + time1 + "ms")
		assertTrue(time1 < 2000, "Extra low performance: $time1 ms")
	}

	@Test
	fun cachedNoisePerformanceTest() {
		val (time2, result2) = executeAndMeasure {
			(1..1).forEach { seed ->
				val dimension = Vector2i(1024)
				val noise = ValueNoise2d(scale = Vector3d(1.0/256.0, 1.0/256.0, seed.toDouble()), interpolateFunction = hermiteInterpolate)
				val cNoise = CachedValueNoise2d(noise, Box2i.byDimension(Vector2i.ZERO, dimension))
				DoubleMap2D.createByXY(dimension) {
					cNoise.output(it.x.toDouble(), it.y.toDouble())
				}
			}
		}

		println("Cached noise 10^6 create for " + time2 + "ms")
		assertTrue(time2 < 1000, "Extra low performance: $time2 ms")

	}
}