package casperix.map2d.bytes

import casperix.math.vector.Vector2i
import casperix.misc.time.executeAndMeasure
import kotlin.test.Test
import kotlin.test.assertEquals

class AlphaMapTest {
	@Test
	fun check() {
		val size = 1024

		val (time, bitmap) = executeAndMeasure {
			val bitmap = AlphaMap(size, size)
			for (x in 0 until bitmap.width) {
				for (y in 0 until bitmap.height) {
					write(bitmap, x, y)
				}
			}
			for (x in 0 until bitmap.width) {
				for (y in 0 until bitmap.height) {
					read(bitmap, x, y)
				}
			}
			bitmap
		}

		println("check-hash-code:${bitmap.hashCode()}")
		println("time: $time ms; size: $size x $size; ")

	}


	private fun writeAndRead(bitmap: AlphaMap, x: Int, y: Int) {
		val value = Alpha(x.toFloat() / y.toFloat())
		bitmap.set(Vector2i(x, y), value)

		val actualValue = bitmap.get(Vector2i(x, y))
		assertEquals(value, actualValue)
	}

	private fun read(bitmap: AlphaMap, x: Int, y: Int) {
		val value = Alpha(x.toFloat() / y.toFloat())
		val actualValue = bitmap.get(Vector2i(x, y))
		assertEquals(value, actualValue)
	}

	@Test
	fun performanceWrite() {
		val size = 1024

		val (time, bitmap) = executeAndMeasure {
			val bitmap = AlphaMap(size, size)
			for (x in 0 until bitmap.width) {
				for (y in 0 until bitmap.height) {
					write(bitmap, x, y)
				}
			}
			bitmap
		}

		println("performance-hash-code:${bitmap.hashCode()}")
		println("time: $time ms; size: $size x $size; ")

	}

	private fun write(bitmap: AlphaMap, x: Int, y: Int) {
		val value = Alpha(x.toFloat() / y.toFloat())
		bitmap.set(Vector2i(x, y), value)
	}


}