package casperix.map2d.bytes

import casperix.math.vector.Vector2i
import casperix.misc.time.executeAndMeasure
import kotlin.test.Test

class PixelMapTest {
	@Test
	fun performanceWrite() {
		val size = 1024

		val (time, bitmap) = executeAndMeasure {
			val bitmap = PixelMap(size, size)
			for (x in 0 until bitmap.width) {
				for (y in 0 until bitmap.height) {
					write(bitmap, x, y)
				}
			}
			bitmap
		}

		println(bitmap.hashCode())
		println("time: $time ms; size: $size x $size; ")

	}

	private fun write(bitmap: PixelMap, x: Int, y: Int) {
		val pixel = Pixel(x.toByte(), y.toByte(), -128, 127)
		val position = Vector2i(x, y)
		bitmap.set(position, pixel)
	}


}