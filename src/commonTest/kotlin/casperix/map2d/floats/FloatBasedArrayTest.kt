package casperix.map2d.floats

import casperix.array1d.floats.FloatBasedArray
import casperix.array1d.floats.FloatCodec
import casperix.math.vector.Vector2f
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails


class FloatBasedArrayTest {

    object SimplePositionCodec : FloatCodec<Vector2f> {
        override fun decode(bytes: FloatArray): Vector2f {
            return Vector2f(bytes[0], bytes[1])
        }

        override fun encode(custom: Vector2f): FloatArray {
            return floatArrayOf(custom.x, custom.y)
        }

        override val floatsPerRecord: Int = 2
    }

    val A = Vector2f(1.1f, -2.2f)
    val B = Vector2f(-3.3f, 4.4f)


    @Test
    fun getAndSet() {

        val array = FloatBasedArray(FloatArray(4), SimplePositionCodec)
        array.set(0, A)
        array.set(1, B)

        assertEquals(A, array.get(0))
        assertEquals(B, array.get(1))
    }

    @Test
    fun exceptionInvalidSize() {
        assertFails {
            FloatBasedArray(FloatArray(5), SimplePositionCodec)
        }
        assertFails {
            FloatBasedArray(FloatArray(-1), SimplePositionCodec)
        }
    }

    @Test
    fun exceptionInvalidAccess() {
        val array = FloatBasedArray(FloatArray(4), SimplePositionCodec)
        assertFails {
            array.set(-1, A)
        }
       assertFails {
            array.set(2, A)
        }
        assertFails {
            array.get(-1)
        }
       assertFails {
            array.get(2)
        }
    }
}