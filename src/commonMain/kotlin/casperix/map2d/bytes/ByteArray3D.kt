package casperix.map2d.bytes

import casperix.math.vector.Vector2i
import casperix.math.vector.Vector3i
import kotlinx.serialization.Serializable

/**
 * Alternative for ByteMap2D, IntMap2D, Map2D, etc.
 * See AlphaMapTest & PixelMapTest  for sample
 */
@Serializable
data class ByteArray3D(val data: ByteArray, val width: Int, val height: Int, val depth: Int) {
	val size = width * height * depth

	constructor(width: Int, height: Int, depth: Int) : this(ByteArray(width * height * depth), width, height, depth)

	init {
		if (data.size != size) throw Error("Expected size is $size, but actual is ${data.size}")
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (other == null || this::class != other::class) return false

		other as ByteArray3D

		if (width != other.width) return false
		if (height != other.height) return false
		if (depth != other.depth) return false
		if (size != other.size) return false
		if (!data.contentEquals(other.data)) return false

		return true
	}

	override fun hashCode(): Int {
		var result = data.contentHashCode()
		result = 31 * result + width
		result = 31 * result + height
		result = 31 * result + depth
		result = 31 * result + size
		return result
	}

	fun setByte(position: Vector3i, value:Byte) {
		val index = indexFromPosition(position)
		data[index]= value
	}

	fun getByte(position: Vector3i):Byte {
		val index = indexFromPosition(position)
		return data[index]
	}

	fun setColumn(position: Vector2i, value: ByteArray) {
		if (value.size != depth) throw Error("Expected size is $depth, but actual is ${value.size}")
		val index = indexFromPosition(position)
		val start = index * depth
		value.copyInto(data, start)
	}


	fun getColumn(position: Vector2i): ByteArray {
		val index = indexFromPosition(position)
		val start = index * depth
		return data.sliceArray(start until start + depth)
	}

	fun indexFromPosition(position: Vector3i): Int {
		if (position.x < 0 || position.y < 0 || position.z < 0 || position.x >= width || position.y >= height || position.z >= depth) throw Error("Position $position is outside box ($width x $height $depth)")
		return (position.x + width * position.y) * depth + position.z
	}

	fun indexFromPosition(position: Vector2i): Int {
		if (position.x < 0 || position.y < 0 ||  position.x >= width || position.y >= height) throw Error("Position $position is outside box ($width x $height)")
		return position.x + width * position.y
	}

	fun positionFromIndex(index: Int): Vector2i {
		if (index < 0 || index >= size) throw Error("Index $index is outside array")
		val x = index % width
		val y = index / width
		return Vector2i(x, y)
	}

}
