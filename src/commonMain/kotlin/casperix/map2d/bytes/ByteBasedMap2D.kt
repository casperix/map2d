package casperix.map2d.bytes

import casperix.map2d.MutableMap2D
import casperix.math.vector.Vector2i

class ByteBasedMap2D<Custom : Any>(val bytes: ByteArray3D, val codec: Codec<Custom>) : MutableMap2D<Custom> {
	override val width = bytes.width
	override val height = bytes.height

	override val dimension: Vector2i get() = Vector2i(width, height)

	override fun get(position: Vector2i): Custom {
		val pixel = bytes.getColumn(position)
		return codec.decode(pixel)
	}

	override fun set(position: Vector2i, value: Custom) {
		val pixel = codec.encode(value)
		bytes.setColumn(position, pixel)
	}
}