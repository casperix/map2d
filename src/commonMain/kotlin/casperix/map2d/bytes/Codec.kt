package casperix.map2d.bytes

interface Codec<Custom> {
	fun decode(bytes: ByteArray): Custom
	fun encode(custom: Custom): ByteArray
}