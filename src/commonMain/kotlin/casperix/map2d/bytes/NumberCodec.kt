package casperix.map2d.bytes


fun Float.toBytes(): ByteArray {
	return toBits().toBytes()
}

fun Float.Companion.fromBytes(value: ByteArray): Float {
	return Float.fromBits(Int.fromBytes(value))
}

fun Int.toBytes(): ByteArray {
	return byteArrayOf(
		(this shr 24).toByte(),
		(this shr 16).toByte(),
		(this shr 8).toByte(),
		(this).toByte(),
	)

}

fun Int.Companion.fromBytes(bytes: ByteArray): Int {
	return ((bytes[3].toInt()) and 0x000000ff) or
			((bytes[2].toInt() shl 8) and 0x0000ff00) or
			((bytes[1].toInt() shl 16) and 0x00ff0000) or
			((bytes[0].toInt() shl 24) and 0xff000000.toInt())
}

fun Long.toBytes(): ByteArray {
	return byteArrayOf(
		(this shr 56).toByte(),
		(this shr 48).toByte(),
		(this shr 40).toByte(),
		(this shr 32).toByte(),
		(this shr 24).toByte(),
		(this shr 16).toByte(),
		(this shr 8).toByte(),
		(this).toByte(),
	)

}
