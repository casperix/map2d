package casperix.map2d

import casperix.map2d.IndexedMap2D.Companion.positionFromIndex
import casperix.math.vector.Vector2i
import kotlinx.serialization.Serializable
import kotlinx.serialization.protobuf.ProtoIntegerType
import kotlinx.serialization.protobuf.ProtoType

@Serializable
data class ShortMap2D(
	override val dimension: Vector2i, @ProtoType(ProtoIntegerType.SIGNED) val array: ShortArray
) : IndexedMap2D<Short>, MutableMap2D<Short> {

	override fun setByIndex(index: Int, value: Short) {
		array[index] = value
	}

	override fun getByIndex(index: Int): Short {
		return array[index]
	}

	companion object {
		fun create(dimension: Vector2i, builder: (index: Int) -> Short): ShortMap2D {
			return ShortMap2D(dimension, ShortArray(dimension.x * dimension.y) { builder(it) })
		}

		fun createByXY(dimension: Vector2i, builder: (pos: Vector2i) -> Short): ShortMap2D {
			return ShortMap2D(dimension, ShortArray(dimension.x * dimension.y) {
				val pos = positionFromIndex(dimension.x, it)
				builder(pos)
			})
		}
	}

	init {
		if (array.size != dimension.x * dimension.y) throw Error("Invalid array size. Need: ${dimension.x * dimension.y}")
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (other == null || this::class != other::class) return false

		other as ShortMap2D

		if (!array.contentEquals(other.array)) return false
		if (dimension != other.dimension) return false

		return true
	}

	override fun hashCode(): Int {
		var result = array.contentHashCode()
		result = 31 * result + dimension.hashCode()
		return result
	}


}