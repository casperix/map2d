package casperix.map2d

import casperix.math.vector.Vector2i

/**
 * representation of each cell of a two-dimensional map by a specific linear array index
 */
interface IndexedMap2D<Cell : Any> : MutableMap2D<Cell> {
	override val dimension: Vector2i

	fun setByIndex(index: Int, value: Cell)

	fun getByIndex(index: Int): Cell


	override fun get(position: Vector2i): Cell {
		return getByIndex(indexFromPosition(position))
	}

	override fun set(position: Vector2i, value: Cell) {
		return setByIndex(indexFromPosition(position), value)
	}


	fun indexFromPosition(pos: Vector2i): Int {
		return pos.x + dimension.x * pos.y
	}

	fun positionFromIndex(index: Int): Vector2i {
		val x = index % dimension.x
		val y = index / dimension.x
		return Vector2i(x, y)
	}

	fun indexFromPosition(x: Int, y: Int): Int {
		return x + dimension.x * y
	}

	companion object {
		fun indexFromPosition(width: Int, pos: Vector2i): Int {
			return pos.x + width * pos.y
		}

		fun positionFromIndex(width: Int, index: Int): Vector2i {
			val x = index % width
			val y = index / width
			return Vector2i(x, y)
		}
	}
}
