package casperix.map2d

import casperix.math.function.Function2D
import casperix.math.vector.Vector2i

import kotlin.math.roundToInt

class FunctionCache(val source: Function2D, val cache: CustomMap2D<Double>) {
	val get: Function2D = { x, y ->
		val value = source(x, y)
		val pos = Vector2i(x.roundToInt(), y.roundToInt())
		if (cache.isInside(pos)) {
			cache.set(pos, value)
		}
		value
	}
}