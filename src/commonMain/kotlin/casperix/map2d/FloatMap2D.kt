package casperix.map2d

import casperix.math.vector.Vector2i


@kotlinx.serialization.Serializable
data class FloatMap2D(override val dimension: Vector2i, val array: FloatArray) : IndexedMap2D<Float>, MutableMap2D<Float> {

	override fun setByIndex(index: Int, value: Float) {
		array[index] = value
	}

	override fun getByIndex(index: Int): Float {
		return array[index]
	}

	companion object {
		fun create(dimension: Vector2i): FloatMap2D {
			return FloatMap2D(dimension, FloatArray(dimension.x * dimension.y))
		}

		fun create(dimension: Vector2i, builder: (index: Int) -> Float): FloatMap2D {
			return FloatMap2D(dimension, FloatArray(dimension.x * dimension.y) { builder(it) })
		}

		fun createByXY(dimension: Vector2i, builder: (pos: Vector2i) -> Float): FloatMap2D {
			var x = -1
			var y = 0
			val array = FloatArray(dimension.x * dimension.y) {
				x++
				if (x == dimension.x) {
					x = 0
					y++
				}
				builder(Vector2i(x, y))
			}

			return FloatMap2D(dimension, array)
		}
	}

	init {
		if (array.size != dimension.x * dimension.y) throw Error("Invalid array size. Need: ${dimension.x * dimension.y}")
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (other == null || this::class != other::class) return false

		other as FloatMap2D

		if (!array.contentEquals(other.array)) return false
		if (dimension != other.dimension) return false

		return true
	}

	override fun hashCode(): Int {
		var result = array.contentHashCode()
		result = 31 * result + dimension.hashCode()
		return result
	}


}