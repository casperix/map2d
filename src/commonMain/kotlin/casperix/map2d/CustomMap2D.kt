package casperix.map2d

import casperix.math.vector.Vector2i
import kotlinx.serialization.Serializable

@Serializable
data class CustomMap2D<T:Any>(override val dimension: Vector2i, val array: MutableList<T>) : IndexedMap2D<T>, MutableMap2D<T> {

	override fun setByIndex(index: Int, value: T) {
		array[index] = value
	}

	override fun getByIndex(index: Int): T {
		return array[index]
	}

	companion object {
		fun <Custom:Any> create(dimension: Vector2i, builder: (index: Int) -> Custom): CustomMap2D<Custom> {
			val items = ArrayList<Custom>(dimension.x * dimension.y)
			for (index in 0 until dimension.x * dimension.y) {
				items.add(builder(index))
			}
			return CustomMap2D(dimension, items)
		}

		fun <Custom:Any> createByXY(dimension: Vector2i, builder: (position: Vector2i) -> Custom): CustomMap2D<Custom> {
			val items = ArrayList<Custom>(dimension.x * dimension.y)
			for (y in 0 until dimension.y) {
				for (x in 0 until dimension.x) {
					val position = Vector2i(x, y)
					items.add(builder(position))
				}
			}
			return CustomMap2D(dimension, items)
		}

	}

	init {
		if (array.size != dimension.x * dimension.y) throw Error("Invalid array size. Need: ${dimension.x * dimension.y}")
	}

	fun <R:Any> map(convertor: (T) -> R): CustomMap2D<R> {
		return create(dimension) { index ->
			val item = array[index]
			convertor(item)
		}
	}

	fun forEach(next: (Vector2i, T) -> Unit) {
		array.forEachIndexed { index, value ->
			val position = positionFromIndex(index)
			next(position, value)
		}
	}
}