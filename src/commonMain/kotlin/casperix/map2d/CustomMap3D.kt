package casperix.map2d

import casperix.math.vector.Vector3i

class CustomMap3D<T:Any>(val dimension: Vector3i, private val array: Array<T>)  {
	companion object {
		inline fun <reified T : Any> create(dimension: Vector3i, builder: () -> T): CustomMap3D<T> {
			return CustomMap3D(dimension, Array(dimension.x * dimension.y * dimension.z) { builder() })
		}
	}

	init {
		if (array.size != dimension.x * dimension.y) throw Error("Invalid array size. Need: ${dimension.x * dimension.y * dimension.z}")
	}

	fun isOutside(position: Vector3i): Boolean {
		return !position.greaterOrEq(Vector3i.ZERO) || !position.less(dimension)
	}

	fun isInside(position: Vector3i): Boolean {
		return position.greaterOrEq(Vector3i.ZERO) && position.less(dimension)
	}

	fun set(position: Vector3i, value: T) {
		array[getPositionIndex(position)] = value
	}

	fun get(position: Vector3i): T {
		return array[getPositionIndex(position)]
	}

	private fun getPositionIndex(pos: Vector3i): Int {
		return pos.x + dimension.x * (pos.y + dimension.y * pos.z)
	}

	fun forEach(op: (T) -> Unit) {
		for (x in 0 until dimension.x) {
			for (y in 0 until dimension.y) {
				for (z in 0 until dimension.z) {
					op(get(Vector3i(x, y, z)))
				}
			}
		}
	}

}