package casperix.map2d

import casperix.math.vector.Vector2i

interface Map2D<Cell:Any> : Collection<Cell> {
	val dimension: Vector2i

	fun get(position: Vector2i): Cell

	val width: Int get() = dimension.x
	val height: Int get() = dimension.y



	fun isOutside(position: Vector2i): Boolean {
		return !position.greaterOrEq(Vector2i.ZERO) || !position.less(dimension)
	}

	fun isInside(position: Vector2i): Boolean {
		return position.greaterOrEq(Vector2i.ZERO) && position.less(dimension)
	}

	fun isBorder(position: Vector2i): Boolean {
		return isBorder(position.x, position.y)
	}

	fun isBorder(x:Int, y:Int): Boolean {
		if ((x == 0 || x == width - 1) && (y >= 0 && y <= height - 1)) {
			return true
		}
		if ((y == 0 || y == height - 1) && (x >= 0 && x <= width - 1)) {
			return true
		}
		return false
	}

	fun get(x:Int, y:Int): Cell {
		return get(Vector2i(x, y))
	}

	fun getOrNull(position: Vector2i): Cell? {
		if (isOutside(position)) return null
		return get(position)
	}


	fun iterate(op: (Int, Int, Cell) -> Unit) {
		for (x in 0 until dimension.x) {
			for (y in 0 until dimension.y) {
				val value = get(x, y)
				op(x, y, value)
			}
		}
	}

	private fun first(op: (Cell) -> Boolean): Boolean {
		for (x in 0 until dimension.x) {
			for (y in 0 until dimension.y) {
				if (op(get(x, y))) {
					return true
				}
			}
		}
		return false
	}

	private fun last(op: (Cell) -> Boolean): Boolean {
		for (x in dimension.x - 1 downTo 0) {
			for (y in dimension.y - 1 downTo 0) {
				if (op(get(x, y))) {
					return true
				}
			}
		}
		return false
	}

	override val size: Int
		get() = dimension.x * dimension.y

	override fun contains(element: Cell): Boolean {
		return first {
			it == element
		}
	}

	override fun containsAll(elements: Collection<Cell>): Boolean {
		return toList().containsAll(elements)
	}

	override fun isEmpty(): Boolean {
		return size == 0
	}

	override fun iterator(): Iterator<Cell> {
		return toList().iterator()
	}

	fun toList(): List<Cell> {
		val list = mutableListOf<Cell>()
		first {
			list.add(it)
			false
		}
		return list
	}

}