package casperix.map2d.perlin

import casperix.map2d.DoubleMap2D
import casperix.math.axis_aligned.Box2i
import casperix.math.vector.Vector2i
import casperix.math.function.Function2D
import casperix.math.function.perlin.ValueNoise2d
import casperix.math.interpolation.hermiteInterpolate

class CachedValueNoise2d(val source: ValueNoise2d, val area: Box2i) {

	class CacheInfo(val octave: ValueNoise2d.OctaveCache, val map: DoubleMap2D, val offset: Vector2i)

	init {
		if (source.interpolateFunction != hermiteInterpolate)
			throw Error("Support only hermite interpolate")
	}

	val normalizer = source.normalizer
	val octaves = Array(source.octaves.size) { index ->
		val octave = source.octaves[index]
		createRandomCache(octave)
	}

	private fun createRandomCache(octave: ValueNoise2d.OctaveCache): CacheInfo {
		val minX = (area.min.x * octave.frequencyX).toInt()
		val minY = (area.min.y * octave.frequencyY).toInt()

		val maxX = (area.max.x * octave.frequencyX).toInt() + 1
		val maxY = (area.max.y * octave.frequencyY).toInt() + 1


		val map = DoubleMap2D.createByXY(Vector2i(maxX - minX + 1, maxY - minY + 1)) {
			ValueNoise2d.randomWithFactor(it.x + minX, it.y + minY, octave.primeNumber)
		}

		return CacheInfo(octave, map, Vector2i(minX, minY))
	}

	val output: Function2D = { x, y ->
		var total = 0.0
		for (cache in octaves) {
			val octave = cache.octave
			total += interpolate2dFlat(x * octave.frequencyX - cache.offset.x, y * octave.frequencyY - cache.offset.y, cache.map) * octave.amplitude
		}
		total * normalizer
	}


	private fun interpolateFunction(a: Double, b: Double, x: Double): Double {
		return a + (b - a) * (3.0 - 2.0 * x) * x * x
	}

	private fun interpolate2dFlat(fx: Double, fy: Double, randoms: DoubleMap2D): Double {
		val baseX = fx.toInt()
		val fractionalX = fx - baseX

		val baseY = fy.toInt()
		val fractionalY = fy - baseY

		val directArray = randoms.array
		val firstIndex = baseX + randoms.width * baseY

		val v1 = directArray[firstIndex]
		val v2 = directArray[firstIndex + 1]
		val v3 = directArray[firstIndex + randoms.width]
		val v4 = directArray[firstIndex + 1 + randoms.width]

		val i1 = interpolateFunction(v1, v2, fractionalX)
		val i2 = interpolateFunction(v3, v4, fractionalX)
		return interpolateFunction(i1, i2, fractionalY)
	}
}