package casperix.array1d.floats

import casperix.array1d.MutableArray1D
import casperix.math.iteration.map


class FloatBasedArray<Record : Any>(val array: FloatArray, private val codec: FloatCodec<Record>) : MutableArray1D<Record> {
    private val floatsPerRecord = codec.floatsPerRecord
    override val size = array.size / floatsPerRecord

    constructor(recordsAmount: Int, codec: FloatCodec<Record>) : this(
        FloatArray(recordsAmount * codec.floatsPerRecord),
        codec
    )

    init {
        if (array.size % floatsPerRecord != 0) {
            throw Exception("size: ${array.size}; floatsPerVertex: $floatsPerRecord. Not compatibled")
        }
    }

    override operator fun get(index: Int): Record {
        val offset = index * floatsPerRecord
        val pixel = array.sliceArray(offset until offset + floatsPerRecord)
        return codec.decode(pixel)
    }

    override operator fun set(index: Int, value: Record) {
        val pixel = codec.encode(value)
        val offset = index * floatsPerRecord
        pixel.copyInto(array, offset)
    }

    override fun iterator(): Iterator<Record> {
        return (0 until size).iterator().map { get(it) }
    }
}