package casperix.array1d.floats

interface FloatCodec<Custom> {
	fun decode(bytes: FloatArray): Custom
	fun encode(custom: Custom): FloatArray

	val floatsPerRecord:Int
}