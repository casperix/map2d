package casperix.array1d

interface Array1D<Item : Any> {
    val size:Int
    operator fun get(index: Int): Item
    operator fun iterator(): Iterator<Item>
}

interface MutableArray1D<Item : Any> : Array1D<Item> {
    operator fun set(index: Int, value: Item)
}
